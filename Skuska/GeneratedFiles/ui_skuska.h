/********************************************************************************
** Form generated from reading UI file 'skuska.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SKUSKA_H
#define UI_SKUSKA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SkuskaClass
{
public:
    QAction *actionVOLBA1;
    QAction *actionOtvor_Playlist;
    QAction *actionVymazanie;
    QAction *actionVypocitaj_cas;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QListWidget *listWidget;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QMenu *menuVypis_pesniciek;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *SkuskaClass)
    {
        if (SkuskaClass->objectName().isEmpty())
            SkuskaClass->setObjectName(QStringLiteral("SkuskaClass"));
        SkuskaClass->resize(1126, 817);
        actionVOLBA1 = new QAction(SkuskaClass);
        actionVOLBA1->setObjectName(QStringLiteral("actionVOLBA1"));
        actionOtvor_Playlist = new QAction(SkuskaClass);
        actionOtvor_Playlist->setObjectName(QStringLiteral("actionOtvor_Playlist"));
        actionVymazanie = new QAction(SkuskaClass);
        actionVymazanie->setObjectName(QStringLiteral("actionVymazanie"));
        actionVypocitaj_cas = new QAction(SkuskaClass);
        actionVypocitaj_cas->setObjectName(QStringLiteral("actionVypocitaj_cas"));
        centralWidget = new QWidget(SkuskaClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        gridLayout->addWidget(listWidget, 2, 0, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 1, 0, 1, 1);

        SkuskaClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(SkuskaClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1126, 26));
        menuVypis_pesniciek = new QMenu(menuBar);
        menuVypis_pesniciek->setObjectName(QStringLiteral("menuVypis_pesniciek"));
        SkuskaClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(SkuskaClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        SkuskaClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(SkuskaClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        SkuskaClass->setStatusBar(statusBar);

        menuBar->addAction(menuVypis_pesniciek->menuAction());
        menuVypis_pesniciek->addAction(actionOtvor_Playlist);
        menuVypis_pesniciek->addAction(actionVymazanie);
        menuVypis_pesniciek->addAction(actionVypocitaj_cas);

        retranslateUi(SkuskaClass);
        QObject::connect(actionOtvor_Playlist, SIGNAL(triggered()), SkuskaClass, SLOT(click1()));
        QObject::connect(actionVymazanie, SIGNAL(triggered()), SkuskaClass, SLOT(click2()));
        QObject::connect(pushButton, SIGNAL(clicked()), SkuskaClass, SLOT(click3()));

        QMetaObject::connectSlotsByName(SkuskaClass);
    } // setupUi

    void retranslateUi(QMainWindow *SkuskaClass)
    {
        SkuskaClass->setWindowTitle(QApplication::translate("SkuskaClass", "Skuska", 0));
        actionVOLBA1->setText(QApplication::translate("SkuskaClass", "Otvor Playlist", 0));
        actionOtvor_Playlist->setText(QApplication::translate("SkuskaClass", "Otvor Playlist", 0));
        actionVymazanie->setText(QApplication::translate("SkuskaClass", "Vymazanie", 0));
        actionVypocitaj_cas->setText(QApplication::translate("SkuskaClass", "Vypocitaj cas", 0));
        pushButton->setText(QApplication::translate("SkuskaClass", "PushButton", 0));
        menuVypis_pesniciek->setTitle(QApplication::translate("SkuskaClass", "Vypis pesniciek", 0));
    } // retranslateUi

};

namespace Ui {
    class SkuskaClass: public Ui_SkuskaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SKUSKA_H
